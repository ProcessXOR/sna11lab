import io

from fastapi import FastAPI, File, UploadFile, Response
from PIL import Image


def rotate_image(image_path, degrees):
    img = Image.open(image_path)
    rotated_img = img.rotate(degrees)
    rotated_img.save('rotated_image.jpg')


app = FastAPI()


def do_rotation(img: Image, degrees: int):
    rotated_img = img.rotate(degrees)
    img_bytes = io.BytesIO()
    rotated_img.save(img_bytes, format='JPEG')
    img_bytes.seek(0)
    return img_bytes


@app.post("/rotateimage/")
async def rotate_image(file: UploadFile = File(...), degrees: int = 90):
    contents = await file.read()
    img = Image.open(io.BytesIO(contents))
    img_bytes = do_rotation(img, degrees)
    return Response(content=img_bytes.read(), media_type="image/jpeg")
