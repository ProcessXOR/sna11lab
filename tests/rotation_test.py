import unittest
from PIL import Image, ImageChops
from src import do_rotation


class MyTestCase(unittest.TestCase):

    @staticmethod
    def are_same(image1, image2):
        diff = ImageChops.difference(image1, image2)

        if diff.getbbox():
            return False
        else:
            return True

    def test_rotation(self):
        test_case = Image.open("tests/case.jpeg")
        obtained = Image.open(do_rotation(test_case, 90))

        desired = Image.open("tests/desired.jpg")
        self.assertTrue(MyTestCase.are_same(desired, obtained))  # add assertion here


if __name__ == '__main__':
    unittest.main()
